##  PreRequisites
Install pip :

Descargar el fichero get-pip.py desde la url https://pip.pypa.io/en/latest/installing.html

```bash
python get-pip.py
sudo pip install virtualenv
```

Y como editor, yo uso PyCharm, que hay una versión libre, con eso nos vale : 
https://www.jetbrains.com/pycharm/download/

# Antes de meternos con Django ...
Vamos a jugar un poco con python, abrid en el navegador el html que hay en la raíz del proyecto CheatSheetPython.html veréis desde variables y listas hasta definición de clases,
para abrir la consola de python basta con teclear python en la consola, o bien abrid el PyCharm con un fichero nuevo, y en la pestaña de abajo donde pone Python Console

```bash
python
```

![Captura de pantalla 2015-09-04 a las 11.08.45.png](https://bitbucket.org/repo/eX65pX/images/2386084119-Captura%20de%20pantalla%202015-09-04%20a%20las%2011.08.45.png)

![Captura de pantalla 2015-09-04 a las 11.13.33.png](https://bitbucket.org/repo/eX65pX/images/2743818252-Captura%20de%20pantalla%202015-09-04%20a%20las%2011.13.33.png)


##  1. Virtualenv

Creamos directorio nuevo, donde crearemos un entorno virtual para el projecto:

```bash
mkdir pylab
cd pylab
virtualenv env
```

![1.png](https://bitbucket.org/repo/eX65pX/images/2291389224-1.png)


##  2. Activamos entorno virtual

```bash
source env/bin/activate
```
![2.png](https://bitbucket.org/repo/eX65pX/images/3105606458-2.png)

##  3. Instalar Django

```bash
pip install Django
```
![3.png](https://bitbucket.org/repo/eX65pX/images/368198717-3.png)

##  4. Creamos el proyecto
Un proyecto puede tener muchas aplicaciones 

```bash
django-admin startproject uzpylab
```

![4.png](https://bitbucket.org/repo/eX65pX/images/88844734-4.png)

Ahora abrimos con el editor de texto, en este caso PyCharm : open project, directorio pylab

![5.png](https://bitbucket.org/repo/eX65pX/images/4264288879-5.png)

##  5. Creamos Aplicación

```bash
cd uzpylab
python manage.py startapp api
```

![6.png](https://bitbucket.org/repo/eX65pX/images/1465349528-6.png)

Veréis que el navegador de archivos se ha actualizado con un nuevo directorio

![7.png](https://bitbucket.org/repo/eX65pX/images/3675638797-7.png)

##  8. Vinculamos aplicación con proyecto
En el fichero `settings.py` añadimos la aplicación con el nombre que la hayamos creado:

```python
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'api'
)
```

![8.png](https://bitbucket.org/repo/eX65pX/images/1137850293-8.png)

##  9. Url's de la API

Veremos un fichero llamado `urls.py` en el proyecto pues añadimos lo siguiente:

```python
url(r'^api/?', include("api.urls"))
```

![9.png](https://bitbucket.org/repo/eX65pX/images/554872394-9.png)

Lo que acabamos de hacer le dice al proyecto como resolver todas las url's que contengan el subdominio /api, ahora es la aplicación la encargada de responder a todo lo que venga después , se maneja mediante otro fichero `urls.py` que vamos a crear dentro del directorio de la aplicación, por ahora lo dejaremos vacío.

![10.png](https://bitbucket.org/repo/eX65pX/images/753401669-10.png)

##  10: DB y usuario

```bash
$ python manage.py syncdb
```

![11.png](https://bitbucket.org/repo/eX65pX/images/1178962771-11.png)

Os pedirá crear un usuario con contraseña, decidle que si, y acordaos para luego de la contraseña :). Como podéis ver si hacéis un ls en la raíz del proyecto os ha creado un fichero sqlite3, que es con el formato que trabaja Django por defecto

![12.png](https://bitbucket.org/repo/eX65pX/images/3896690613-12.png)



##  11. Modelo de datos
En el fichero `models.py` creamos la siguiente clase :

```python
class Disc(models.Model):
    name = models.CharField(max_length=20)
    group = models.CharField(max_length=20)
    year = models.DateField(auto_now=True)
```
![14.png](https://bitbucket.org/repo/eX65pX/images/3611664873-14.png)

Ahora añadiremos el modelo al admin:

En el fichero `admin.py` añadimos lo siguiente:

```python
from api.models import Disc
admin.site.register(Disc)
```
![15.png](https://bitbucket.org/repo/eX65pX/images/1806375144-15.png)

Ahora sincronizamos la BD que ya tenemos con el nuevo modelo :

```bash
$ python manage.py makemigrations
$ python manage.py migrate
```
![16.png](https://bitbucket.org/repo/eX65pX/images/2999692570-16.png)

y ahora podemos arrancar el servidor:
```bash
$ python manage.py runserver
```
![13.png](https://bitbucket.org/repo/eX65pX/images/2556458368-13.png)

##  12.  Admin
Si habéis entrado a http://127.0.0.1:8000/ veréis que aún no responde a nada, y como estamos haciendo una API rest sencilla, primero vamos a rellenar la BD con algunos datos, se puede hacer bien por consola o bien con una herramienta que proporciona Django muy potente, el admin. Para usarlo como nuestra api aun no tiene url's que resolver comentaremos el módulo donde la hemos añadido para poder acceder al admin, por lo tanto comentad en el fichero `settings.py` la linea que añadimos en el paso 9.

Ahora podréis acceder a http://127.0.0.1:8000/admin con el usuario y contraseña que especificarais al crear la BD veréis algo como esto : 
![17.png](https://bitbucket.org/repo/eX65pX/images/1287289856-17.png)

Ya podemos añadir datos a la BD dandole al icono del + .

## 13. Methods GET, POST, 
Por fin vamos a implementar los 2 métodos de los 5 que llevan las APIs , lo primero de todo en el fichero `views.py`` añadimos lo siguiente por ahora, que nos devolcerá en formato json los datos de la BD:

```python
from django.http import HttpResponse
from django.core import serializers
from api.models import Disc


def disc(request):
    return HttpResponse(serializers.serialize("json", Disc.objects.all()))
```

El import de render no nos hará falta, en caso de querer trabajar con vistas, el return de nuestro método debería ser un rendir, en lugar una response como es ahora.

En el fichero `urls.py` que dejamos vacío al principio ya podemos añadir un end point: 

```python
from django.conf.urls import url
from api.views import disc


urlpatterns = [
    url(r'^disc/?', disc)
]
```
![18.png](https://bitbucket.org/repo/eX65pX/images/123432308-18.png)

*Lo que estamos haciendo aquí, es decirle que todo lo que responda al dominio disc el encargado de responder es el método que tenemos implementado en la view disc.*

Ahora en `views.py` podemos completar el método disc para que responda como queremos, para saber que tipo de petición se nos ha hecho consultamos la request que le llega por parámetro *request.method*

```python
from django.http import HttpResponse
from django.core import serializers
from api.models import Disc

def disc(request):
    print("metodo disc")
    if request.method == 'GET':
       response = get()
    if request.method == 'POST':
        print(request.POST)
        response = post(request.POST)
    return response


def get():
    print("metodo get")
    return HttpResponse(serializers.serialize("json", Disc.objects.all()))


def post(data):
    print("method post")
    disc = Disc(name=data['name'], group=data['group'], year=data['year'])
    disc.save()
    return HttpResponse("ok")
```
![20.png](https://bitbucket.org/repo/eX65pX/images/179471974-20.png)

*Para probar el método get acceder a : http://127.0.0.1:8000/api/disc/

*Para probar el post podemos probar con la extensión de chrome Advance Rest Client Aplication
![22.png](https://bitbucket.org/repo/eX65pX/images/3864085501-22.png)

*Si quisiéramos implementar el resto de métodos de la API necesitamos el método request.read(), que retorna los datos en formato QUERY_STRING *

*Por último una pequeña curiosidad, si en el entorno virtual ejecutáis `pip freeze > libs.txt` tendremos una lista actualizada con las librerías y versiones que utiliza tu proyecto :) *