from django.db import models


# Create your models here.
class Disc(models.Model):
    name = models.CharField(max_length=20)
    group = models.CharField(max_length=20)
    year = models.DateField(auto_now=True)
