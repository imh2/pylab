from django.http import HttpResponse
from django.core import serializers
from api.models import Disc


# Create your views here.
def disc(request):
    print("metodo disc")
    if request.method == 'GET':
       response = get()
    if request.method == 'POST':
        print(request.POST)
        response = post(request.POST)
    return response


def get():
    print("metodo get")
    return HttpResponse(serializers.serialize("json", Disc.objects.all()))


def post(data):
    print("method post")
    disc = Disc(name=data['name'], group=data['group'], year=data['year'])
    disc.save()
    return HttpResponse("ok")

