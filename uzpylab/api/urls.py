from django.conf.urls import url
from api.views import disc

urlpatterns = [
    url(r'^disc/?', disc)
]
